const ReactDOM = require('react-dom');
const React = require('react');
const LazyLoad = require('react-lazy-load');
const Axios = require('axios');

require('./styles.css');

const Youtube = React.createClass({
    render: function () {
        const videoUrl = "https://www.youtube.com/embed/"+this.props.videoId;
        return(
            <iframe
                width="100%"
                height="500"
                src={videoUrl}
                frameBorder="0"
                allowFullScreen>
            </iframe>
        )
    }
});

const VideoItem = React.createClass({
    render: function () {

        return(
                <LazyLoad height={250}>
                        <img src={this.props.video.thumb} alt="" onClick={this.props.onClickThumb} />
                </LazyLoad>

        )
    }
});

const VideoList = React.createClass({
    render: function () {
        let onClickThumb = this.props.onClickThumb
        return(
            <div>
                {
                    this.props.Data.map(function (item, index) {
                        return(
                            <VideoItem key={index} video={item} onClickThumb={onClickThumb.bind(null, item)} />
                        )
                    })
                }
            </div>
        )
    }
});

const Comments = React.createClass({
    getInitialState: function () {
        return{
            comments: [],
            text: ''
        }
    },
    componentDidMount: function() {
        let localComments = JSON.parse(localStorage.getItem('comments'));
        if (localComments.length) {
            this.setState({ comments: localComments });
        }
        this.setState({ text: '' });
    },
    componentDidUpdate: function() {
        this._updateLocalStorage();
    },
    handleTextChange: function(event) {
        this.setState({ text: event.target.value });
    },

    handleCommentAdd: function() {
        const newComment = {
            text: this.state.text,
            key: Date.now()
        };
        let newComments = this.state.comments.slice();
        if(!newComments[this.props.video]) newComments[this.props.video] = [];
        newComments[this.props.video].unshift(newComment);
        this.setState({ comments: newComments });
        this.setState({ text: '' });
    },
    _updateLocalStorage: function() {
        let comments = JSON.stringify(this.state.comments);
        localStorage.setItem('comments', comments);
    },

    render: function () {
        let currentComments = this.state.comments[this.props.video];
        if(!currentComments) currentComments = [];
        return(
            <div className="row">
                <div className="comments">
                    {
                        currentComments.map(function(comment){
                            return (
                                <p>{comment.text}</p>
                            );
                        })
                    }
                </div>
                <div className="form-group">
                    <textarea className="form-control" onChange={this.handleTextChange} value={this.state.text} />
                </div>
                <button onClick={this.handleCommentAdd} className="btn btn-primary" >Add comment</button>

            </div>
        )
    }
});

const EskimoApp = React.createClass({
    getInitialState: function () {
        return{
            currentVideo: 0,
            prevButtonVisible: 'none',
            nextButtonVisible: 'none',
            Data: []
        }
    },
    componentWillMount: function() {
        this._checkArrowsVisible();
    },
    componentDidMount: function () {
        Axios.get(`eskimo.json`)
            .then(res => {
                const videos = res.data;
                this.setState({ Data: videos });
                this._checkArrowsVisible();
            });
    },
    componentWillUpdated: function() {
        this._checkArrowsVisible(this.props.currentVideo);
    },
    handleNextVideo: function () {
        let nextVideo = this.state.currentVideo + 1;
        if (nextVideo !== undefined){
            console.log(nextVideo);
            this.setState({currentVideo: nextVideo});
            this._checkArrowsVisible(nextVideo);
        }

    },
    handlePrevVideo: function () {
        let prevVideo = this.state.currentVideo - 1;
        console.log(prevVideo);
        if (prevVideo !== undefined){
            console.log(prevVideo);
            this.setState({currentVideo: prevVideo});
            this._checkArrowsVisible(prevVideo);
        }

    },
    handleClickVideoThumb: function (index) {
        this.setState({currentVideo: this.state.Data.indexOf(index)});
    },
    _checkArrowsVisible: function (id = 0) {
        if(id > 0){
            this.setState({prevButtonVisible: 'block'})
        } else {
            this.setState({prevButtonVisible: 'none'})
        }
        console.log((id < this.state.Data.length));
        if(id < (this.state.Data.length-1)){
            this.setState({nextButtonVisible: 'block'});
        } else {
            this.setState({nextButtonVisible: 'none'});
        }
    },

    render: function () {
        let currentVideo = this.state.currentVideo;
        let title = this.state.Data[currentVideo]? this.state.Data[currentVideo]['title'] : '';
        let videoId = this.state.Data[currentVideo]? this.state.Data[this.state.currentVideo]['v'] : '';
        return(
            <div className="player">
                <div className="row">
                    <div className="col-sm-9">
                        <div className="row">
                            <div className="col-xs-12 text-center">
                                <h2>{currentVideo+1}/{this.state.Data.length}</h2>
                                <h1>{title}</h1>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-1 text-center">
                                <h1 style={{display: this.state.prevButtonVisible}} onClick={this.handlePrevVideo}><span className="glyphicon glyphicon-menu-left" aria-hidden="true"> </span></h1>
                            </div>
                            <div className="col-md-10">
                                <Youtube videoId={videoId} />
                            </div>
                            <div className="col-md-1 text-center">
                                <h1 style={{display: this.state.nextButtonVisible}} onClick={this.handleNextVideo}><span className="glyphicon glyphicon-menu-right" aria-hidden="true"> </span></h1>
                            </div>
                        </div>
                        <Comments video={this.state.currentVideo}/>
                    </div>
                    <div className="col-sm-3">
                        <VideoList Data={this.state.Data} onClickThumb={this.handleClickVideoThumb} />
                    </div>
                </div>

            </div>
        )
    }
});


ReactDOM.render(
<EskimoApp />,
    document.getElementById('mount-point')
);